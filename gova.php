<?php

/**
 * Plugin Name: RS-Tools
 * Description: Agrega custom posts y funcionalidades al sitio rssindical.mx
 * Version: 1.0
 * Author: Alfredo Gonzalez
 * Author URI:
 * License: GLP2
 * License URI: https://www.gnu.com/licenses/gpl-2.0.html
 */

global $wpdb;

defined('ABSPATH') or die('No script kiddies please!');

define('RS_DIR', __DIR__ . '/');
define('RS_URL', plugins_url('/', __FILE__));

add_action('admin_enqueue_scripts','admin_script');//funciones en el admin

function admin_script(){
    wp_enqueue_style( 'rs',RS_URL.'assets/css/main.css' );
}

// verifica que exista la pagina de revista digital
if(get_page_by_title('revistas') == null){
    create_pages_fly('Revistas');
}

function create_pages_fly($pageName){
    $page = array(
        'post_title'    =>  $pageName,
        'post_content'  =>  'Revistas digitales',
        'post_status'   =>  'publish',
        'post_author'   =>  1,
        'post_type'     =>  'page',
        'post_name'     =>  $pageName
    );
    wp_insert_post($page);
}



function crea_cp_revista(){
    $labels = array(
		'name'                => _x( 'Revistas', 'Post Type General Name', 'revista' ),
		'singular_name'       => _x( 'Revista', 'Post Type Singular Name', 'revista' ),
		'menu_name'           => __( 'Revistas', 'revista' ),
		'parent_item_colon'   => __( 'Superior:', 'revista' ),
		'all_items'           => __( 'Todas las revistas', 'revista' ),
		'view_item'           => __( 'Ver Revistas', 'revista' ),
		'add_new_item'        => __( 'Agregar Revista', 'revista' ),
		'add_new'             => __( 'Agregar Revista', 'revista' ),
		'edit_item'           => __( 'Editar Revista', 'revista' ),
		'update_item'         => __( 'Actualizar', 'revista' ),
		'search_items'        => __( 'Buscar', 'revista' ),
		'not_found'           => __( 'No se ha encontrado', 'revista' ),
		'not_found_in_trash'  => __( 'Nada en la pepelera', 'revista' ),
	);
	$rewrite = array(
		'slug'                => 'revistas',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
    );

	$args = array(
		'label'               => __( 'Revistas', 'revista' ),
		'description'         => __( 'Revistas', 'revista' ),
		'labels'              => $labels,
		'supports'            => array( 'title','thumbnail'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 12,
		'menu_icon'           => 'dashicons-media-text',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
    );

	register_post_type( 'Revistas', $args );
}

// Hook into the 'init' action for CPT revistas
add_action( 'init', 'crea_cp_revista', 0 );

// function admin_styles() {
//     wp_enqueue_style('thickbox');
// }
// add_action('admin_print_styles', 'admin_styles');

// Metabox para adjuntar PDF
function add_custom_metaboxes(){
    add_meta_box( 'my-pdf', 'Revista Digital PDF', 'pdf_link', 'revistas', 'normal' );
}
add_action('add_meta_boxes','add_custom_metaboxes');

// callback metabox
function pdf_link(){
    global $post;

    if (extension_loaded('imagick')) {
        $im = new Imagick();
        $im->setResolution(300,210);
    }
    else die("No existe imagick!!");


  $url = get_post_meta($post->ID, 'pdf_link', true);
  $img_url = get_post_meta($post->ID, 'img_link', true);?>
  <input name="input_text_pdf" class="input_text_metabox" type="hidden" value="<?php echo $url ?>" readonly size="50" /><br/>
  <a id="input_pdf" class="pdf" autocomplete="off" name="input_pdf" type="text">Cargar archivo</a>
  <!--<input id="btn_input_pdf" name="btn_input_pdf" type="button" value="Upload Image" /><br/>-->
  <br />
  <center><img src="<?php echo $img_url;?>" id="picsrc" name="picsrc" /></center>

  <script>
    jQuery('body').on('click', '.pdf', function(e){
        e.preventDefault();
        var button = jQuery(this);
        var custom_uploader = wp.media({
                title: 'Seleccionar PDF',
                library : {
                    // uncomment the next line if you want to attach image to the current post
                    // uploadedTo : wp.media.view.settings.post.id,
                    type : 'application/pdf' // estaba image, es el filtro para mostrar
                },
                button: {
                    text: 'Usar este PDF' // button label text
                },
                multiple: false // for multiple image selection set to true
            }).on('select', function() { // it also has "open" and "close" events
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                var img_url = attachment.url.replace('.pdf','-pdf.jpg');

                jQuery("input[name='input_text_pdf']").val(attachment.url);
                //$(button).removeClass('button').html('<div style="widht:100%;"><img class="pdf" src="'+ img_url + '" style="max-width:18%;display:block; left:42%; position:relative;" /></div>').next().val(attachment.id).next().show();
                jQuery("#picsrc").attr('src',img_url);

            }).open();
    });
  </script>
<?php
}

// hook save post para el pdf
function save_pdf_link() {
    global $post;

    if(!isset($post->ID) || get_post_type($post->ID) !== 'revistas') return;

    $img_url = str_replace('.pdf','-pdf.jpg',$_POST['input_text_pdf']);

    //die($img_url);

    if (!empty($_POST['input_text_pdf'])){
        update_post_meta($post->ID, 'pdf_link', $_POST['input_text_pdf']);
        update_post_meta($post->ID, 'img_link', $img_url);
    }
    else return false;

}
add_action('save_post', 'save_pdf_link');


?>